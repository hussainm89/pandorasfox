// Stage options: controls UI elements in options screen.

using Godot;
using System;
using System.Collections.Generic;

public class StageOptions : Stage
{
	private Panel[] panels;
	private Dictionary<string, Dictionary<string, Control>> settingsDict;
	private CntOptionBtns cntOptionsBtns;

	private Panel currentPanel;

	public override void _Ready()
	{
		base._Ready();

		cntOptionsBtns = (CntOptionBtns)GetNode("CntOptionBtns");

		settingsDict = new Dictionary<string, Dictionary<string, Control>>()
		{
			{"Sound", new Dictionary<string, Control>()
				{
					{"Button", (Button) GetNode("CntOptionBtns/BtnSound")},
					{"Panel", (Panel) GetNode("OptionPnls/PnlSound")}
				}
			},
			{"Controls", new Dictionary<string, Control>()
				{
					{"Button", (Button) GetNode("CntOptionBtns/BtnControls")},
					{"Panel", (Panel) GetNode("OptionPnls/PnlControls")} 
				}
			},
			{"Player", new Dictionary<string, Control>()
				{
					{"Button", (Button) GetNode("CntOptionBtns/BtnPlayer")},
					{"Panel", (Panel) GetNode("OptionPnls/PnlPlayer")} 
				}
			}
		};

		panels = new Panel[] {(Panel)settingsDict["Sound"]["Panel"], (Panel)settingsDict["Controls"]["Panel"], (Panel)settingsDict["Player"]["Panel"]};
		//Start with showing controls
		ShowPanel("Controls");
	}

	private void ShowPanel(string setting)
	{
		if (currentPanel != null)
			((IPnlOptions)currentPanel).Exit();
		currentPanel = (Panel)settingsDict[setting]["Panel"];
		cntOptionsBtns.DisableSingleButton((Button) settingsDict[setting]["Button"]);
		foreach (Panel p in panels)
		{
			p.Hide();
		}
		currentPanel.Show();
		((IPnlOptions)currentPanel).Init();   
	}

	private void _on_BtnMainMenu_pressed()
	{
		((IPnlOptions)currentPanel).Exit();
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}
	
	
	private void _on_BtnPlayer_pressed()
	{
		ShowPanel("Player");
	}
	
	
	private void _on_BtnSound_pressed()
	{
		ShowPanel("Sound");
	}
	
	
	private void _on_BtnControls_pressed()
	{
		ShowPanel("Controls");
	}
}


