using Godot;
using System;
using System.Linq;
using System.Collections.Generic;

// put variable for number of toys and food active. when fox is hungry, it will publish Hungry event
// if food is available, we then remove the food and add to the hungry fox's satiety 

public class StageGame : Stage
{
	// public delegate void SpeedChangedDelegate(int speed);
	// public event SpeedChangedDelegate SpeedChanged;
	public delegate void DidScaryEventDelegate();
	public event DidScaryEventDelegate DidScaryEvent;
	private PictureStory _pictureStory;
	private Label _lblDay;
	private Label _lblTime;
	private Label _lblMoney;
	private Label _lblNextMoney;
	private LblEventInfo _lblEventInfo;
	private TimeHandler _timeHandler;
	private InventoryGrid _inventoryPlayer;
	private InventoryGrid _inventoryShop;
	private TextureRect _texGarden;
	private PackedScene _scnInventoryItem;
	private PackedScene _scnTurret;
	private PackedScene _scnToy;
	private PackedScene _scnFood;
	private PackedScene _scnMedicine;
	private PackedScene _scnPresentFox;
	private PackedScene _scnFox;
	private PackedScene _scnPoo;
	private Control _cntTurrets;
	private Control _cntToys;
	private Control _cntFood;
	private Control _cntMedicine;
	private Control _cntFoxes;
	private Control _cntPoos;
	private Camera2D _camera2D;
	private TextureProgress _prSatisfaction;
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private AudioStreamPlayer _narrationPlayer;
	private Dictionary<AudioData.GameSounds, AudioStream> _gameSounds = AudioData.LoadedSounds<AudioData.GameSounds>(AudioData.GameSoundPaths);
	private int _weeklyWage = 0;
	private int _payPerHour = 50;
	private int _money = 1000;
	private bool _paidToday = false;
	private float _foodHealthConstant = 1f;
	private float _medicineHealthConstant = 1f;
	private float _toyHealthConstant = 1f;
	private int _timeToCleanPoo = 15;
	private int _timeToPetFox = 10;
	private bool _toldOff = false;
	// private float _securityHealthConstant = 1f;
	private List<Vector2> _turretPositions = new List<Vector2>();
	private List<Vector2> _toyPositions = new List<Vector2>();
	private List<Vector2> _foodPositions = new List<Vector2>();
	private List<Vector2> _medicinePositions = new List<Vector2>();
	private List<Vector2> _pooPositions = new List<Vector2>();
	private List<Tuple<Vector2, bool>> _foxPositions = new List<Tuple<Vector2, bool>>();
	private List<InventoryItem> _turrets = new List<InventoryItem>();
	private List<InventoryItem> _foods = new List<InventoryItem>();
	private List<InventoryItem> _toys = new List<InventoryItem>();
	private List<InventoryItem> _medicines = new List<InventoryItem>();
	private List<Fox> _foxes = new List<Fox>();
	private List<Action> _events;
	private ScreenShaker2D _shaker;
	public int _itemUID = 0; // all item construction vars and methods should be in a separate class
	private InventoryItem.ItemClass[] _shopItems = new InventoryItem.ItemClass[5]
	{ InventoryItem.ItemClass.Turret, InventoryItem.ItemClass.Toy, InventoryItem.ItemClass.Food, InventoryItem.ItemClass.PresentFox, InventoryItem.ItemClass.Medicine};
	private List<AudioData.GameSounds> _narrativeLines = new List<AudioData.GameSounds>() {
		AudioData.GameSounds.AttackInNight, AudioData.GameSounds.FoodStolen, AudioData.GameSounds.LosePandora, AudioData.GameSounds.NightFell, 
		AudioData.GameSounds.Vacuum, AudioData.GameSounds.Win, AudioData.GameSounds.FoodPoisoning
	};


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();
		InitNodes();
		_timeHandler.SetSpeed(TimeHandler.SpeedSetting.Quadruple);
		InitItemPositions();
		InitFoxes();
		InitEvents();
		// InitPoos();
		// SetConstants();
		SetMoney(3000);
		ShopRestock();


		
	}

	// private void SetConstants()
	// {
	// 	// _foodHealthConstant = new Random().Next(1,20)*0.1f;
	// }

	private void InitNodes()
	{
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_narrationPlayer = GetNode<AudioStreamPlayer>("NarrationPlayer");
		_pictureStory = GetNode<PictureStory>("HUD/PictureStory");
		_texGarden = GetNode<TextureRect>("TexGarden");
		_timeHandler = GetNode<TimeHandler>("TimeHandler");
		_timeHandler.NewDay+=this.OnNewDay;
		_timeHandler.NewHour+=this.OnNewHour;
		_timeHandler.SpeedChanged+=this.OnSpeedChanged;
		_lblDay = GetNode<Label>("HUD/LblDay");
		_lblTime = GetNode<Label>("HUD/LblTime");
		_lblMoney = GetNode<Label>("HUD/LblMoney");
		_lblNextMoney = GetNode<Label>("HUD/LblNextMoney");
		_lblEventInfo = GetNode<LblEventInfo>("HUD/LblEventInfo");
		_inventoryPlayer = GetNode<InventoryGrid>("HUD/InventoryGrid");
		_inventoryShop = GetNode<InventoryGrid>("HUD/InventoryShop");
		_inventoryPlayer.InventoryClosed+=this.OnInventoryClosed;
		_inventoryShop.InventoryClosed+=this.OnInventoryClosed;
		_cntTurrets = GetNode<Control>("TexGarden/CntTurrets");
		_cntToys = GetNode<Control>("TexGarden/CntToys");
		_cntFood = GetNode<Control>("TexGarden/CntFood");
		_cntMedicine = GetNode<Control>("TexGarden/CntMedicine");
		_cntFoxes = GetNode<Control>("TexGarden/CntFoxes");
		_cntPoos = GetNode<Control>("TexGarden/CntPoos");
		_prSatisfaction = GetNode<TextureProgress>("HUD/PrSatisfaction");
		_scnInventoryItem = (PackedScene)GD.Load("res://Common/Nodes/InventoryGrid/InventoryItem.tscn"); // perhaps put this all in a new class as a dict
		_scnTurret = (PackedScene)GD.Load("res://Entities/Turret/Turret.tscn");
		_scnToy = (PackedScene)GD.Load("res://Entities/Toy/Toy.tscn");
		_scnFood = (PackedScene)GD.Load("res://Entities/Food/Food.tscn");
		_scnMedicine = (PackedScene)GD.Load("res://Entities/Medicine/Medicine.tscn");
		_scnFox = (PackedScene)GD.Load("res://Entities/Fox/Fox.tscn");
		_scnPresentFox = (PackedScene)GD.Load("res://Entities/PresentFox/PresentFox.tscn");
		_scnPoo = (PackedScene)GD.Load("res://Entities/Poo/Poo.tscn");
		_camera2D = GetNode<Camera2D>("Camera2D");
		_shaker = new ScreenShaker2D(_camera2D, duration:0.2f, magnitude:4);
		//res://Entities/Food/Food.tscn
	}

	public override void Init()
	{
		OnBtnSpeedNormalPressed();
		AudioHandler.PlaySound(_musicPlayer, _gameSounds[AudioData.GameSounds.Music], AudioData.SoundBus.Music);
		Intro();
	}

	private void InitItemPositions()
	{
		foreach (Node n in _cntTurrets.GetChildren())
		{
			if (n is Position2D pos2D)
			{
				_turretPositions.Add(pos2D.Position);
			}
		}
		foreach (Node n in _cntToys.GetChildren())
		{
			if (n is Position2D pos2D)
			{
				_toyPositions.Add(pos2D.Position);
			}
		}
		foreach (Node n in _cntFood.GetChildren())
		{
			if (n is Position2D pos2D)
			{
				_foodPositions.Add(pos2D.Position);
			}
		}
		foreach (Node n in _cntMedicine.GetChildren())
		{
			if (n is Position2D pos2D)
			{
				_medicinePositions.Add(pos2D.Position);
			}
		}
		foreach (Node n in _cntPoos.GetChildren())
		{
			if (n is Position2D pos2D)
			{
				_pooPositions.Add(pos2D.Position);
			}
		}
		// foreach (Node n in _cntFoxes.GetChildren()) // this and position2ds for foxes may not be needed as we place foxes initially and then use their positions...
		// {
		// 	if (n is Position2D pos2D)
		// 	{
		// 		_foxPositions.Add(pos2D.Position);
		// 	}
		// }
	}
	#region winlose
	private bool CheckLose()
	{
		if (_foxes.Count == 0)
		{
			return true;
		}
		if (GetSatisfaction() == 0)
		{
			return true;
		}

		return false;
	}

	private bool CheckWin()
	{
		// return false;
		foreach (Fox f in _foxes)
		{
			if (f.GetMaturity() != Fox.State.Adult)
			{
				// GD.Print(f.GetMaturity().ToString());
				// GD.Print("Health: ", f.Health, " Fox ID: ", f.foxID);
				// GD.Print("Happy: ", f.Happiness, " Fox ID: ", f.foxID);
				// GD.Print("Satiety: ", f.Satiety, " Fox ID: ", f.foxID);
				return false;
			}
		}

		if (_foxes.Count < 4)
		{
			return false;
		}

		if (GetSatisfaction() < 95)
		{
			return false;
		}

		return true;
	}

	#endregion

	private int _foxUIDs = 0;

	private void InitFoxes() // this only runs at the start for existing foxes
	{
		foreach (Node n in _cntFoxes.GetChildren())
		{
			if (n is Fox f)
			{
				f.foxID = _foxUIDs;
				_foxes.Add(f);
				ConnectFoxEvents(f);
				_foxUIDs += 1;

				// if (new Random().Next(0,2) == 0)
				// {
				// 	f.SetFlip(true);
				// }
			}
		} 


		
		// foreach (Fox f in _foxes)
		// {
		// 	GD.Print("ID: ", f.foxID);
		// }
		_foxes[0].SetMaturity(Fox.State.Baby);
		_foxes[0].SetFlip(true);
		_foxes[2].SetMaturity(Fox.State.Baby);
		_foxes[3].SetMaturity(Fox.State.Adult);
		_foxes[1].SetMaturity(Fox.State.Adult);
		_foxes[1].SetFlip(true);

		// TESTING PRESENTS:
		// _foxes.Find(x => x.foxID == 0).Visible = false;
		// _foxes.Find(x => x.foxID == 1).Visible = false;
		// _foxes.Find(x => x.foxID == 2).Visible = false;
		// _foxes.Find(x => x.foxID == 3).Visible = false;
		// OnFoxDied(0);
		// OnFoxDied(1);
		// OnFoxDied(2);
		// OnFoxDied(3);

	}

	private void InitEvents()
	{
		_events = new List<Action> {
			EventHoover, EventFoodPoisoning, EventFoodStolen
		};
	}

	private float GetSatisfaction()
	{
		float totalFoxLevel = 0;
		foreach (Fox f in _foxes)
		{
			// GD.Print(f.GetMaturity().ToString());
			totalFoxLevel += (f.Health + f.Happiness + f.Satiety)/3;
		}
		return totalFoxLevel/4;
	}

	private void GetToldOff()
	{
		if (_toldOff)
		{
			return;
		}
		_toldOff = true;
		// AudioHandler.PlaySound(_soundPlayer, _gameSounds[AudioData.GameSounds.BackToWork], AudioData.SoundBus.Voice);
	}
	// private void InitPoos() // we wont do this when foxes poo
	// {
	// 	foreach (Node n in _cntPoos.GetChildren())
	// 	{
	// 		if (n is Poo poo)
	// 		{
	// 			InitPoo(poo);
	// 		}
	// 	}
	// }

	// private void InitPoo(Poo p)
	// {
	// }
	#region foxpoo
	private void OnPooMadeFumes()
	{
		HurtRandomFox(10,30);
		// int index = new Random().Next(0,_foxes.Count);
		// _foxes[index].Health -= new Random().Next(10,30);
		_lblEventInfo.QueueText("Fumes from a poop hurt one of the foxes!", 2);
	}

	private void OnFoxDidPoo()
	{
		Poo poo = (Poo)_scnPoo.Instance();
		_cntPoos.AddChild(poo);
		int index = new Random().Next(0, _pooPositions.Count);
		poo.RectPosition = _pooPositions[index];
		_pooPositions.Remove(_pooPositions[index]);
		// poo.RectPosition = new Vector2(new Random().Next(0, (int)(_texGarden.RectSize.x-poo.GetPooSize().x)), 
		// 	new Random().Next(0, (int)(_texGarden.RectSize.y-poo.GetPooSize().y)));		
		foreach (Fox f in _foxes)
		{
			if (new Random().Next(0,2) == 0)
			{
				poo.MadeFumes+=f.OnExposedToPoo;
			}
		}
		// poo.MadeFumes+=this.OnPooMadeFumes; // this will actually connect to fox.OnExposedToPoo when that is implemented
		poo.PooClicked+=this.OnPooClicked;
		poo.DivideTimerFumesWaitTime(_timeHandler.GetSpeed());
		_lblEventInfo.QueueText("Quickly! Pick up the poop before the foxes get sick!", 1);
		// GD.Print("POO WAIT TIME: ", poo.GetNode<Timer>("TimerFumes").WaitTime);
		poo.Start();
	}

	private void OnSpeedChanged(int speed)
	{
		foreach (Node n in _cntPoos.GetChildren())
		{
			if (n is Poo p)
			{
				p.DivideTimerFumesWaitTime(_timeHandler.GetSpeed());
				p.TimerFumesPaused(false);
				if (_timeHandler.GetSpeed() == 0)
				{
					p.TimerFumesPaused(true);
				}
			}
		}
	}

	private void OnPooClicked(Vector2 pos)
	{
		GetToldOff();
		_pooPositions.Add(pos);
		UpdateTimesheet(-(_payPerHour * _timeToCleanPoo/60));
		_lblEventInfo.QueueText("You leave your desk to clean up some poop! Shh!", 1);
	}
	#endregion

	private void AddFox(Tuple<Vector2, bool> posInfo)//Vector2 position, bool IsFlipped)
	{
		Fox f = (Fox)_scnFox.Instance();
		_foxes.Add(f);
		f.foxID = _foxUIDs;
		_foxUIDs+=1;
		_cntFoxes.AddChild(f);
		f.SetMaturity(Fox.State.Baby);
		f.Position = posInfo.Item1;

		f.SetFlip(posInfo.Item2);
		
		// if (new Random().Next(0,2) == 0)
		// {
		// 	f.SetFlip(true);
		// }

		//f.SetMaturity
		ConnectFoxEvents(f);
		// PositionFox(f);
	}

	private void ConnectFoxEvents(Fox f) // connect other fox events here as needed
	{
		_timeHandler.SpeedChanged+=f.OnSpeedChanged;
		_timeHandler.NewHour+=f.OnNewHour;
		_timeHandler.NewDay+=f.OnNewDay;
		f.FoxDidPoo+=this.OnFoxDidPoo;
		f.OnFoxDied+=this.OnFoxDied;
		f.PetFox+=this.OnFoxPet;
		this.DidScaryEvent+=f.OnScaryEvent;
		foreach (Node n in _cntPoos.GetChildren())
		{
			if (n is Poo p && new Random().Next(0,2) == 0)
			{
				p.MadeFumes+=f.OnExposedToPoo;
			}
		}

		// f.LevelledUpToday = true;
		//_timeHandler.NewHour+=f.OnNewHour;
	}

	private void DisconnectFoxEvents(Fox f) // connect other fox events here as needed
	{
		_timeHandler.SpeedChanged-=f.OnSpeedChanged;
		_timeHandler.NewHour-=f.OnNewHour;
		f.FoxDidPoo-=this.OnFoxDidPoo;
		f.OnFoxDied-=this.OnFoxDied;
		f.PetFox-=this.OnFoxPet;
		this.DidScaryEvent-=f.OnScaryEvent;
		foreach (Node n in _cntPoos.GetChildren())
		{
			if (n is Poo poo)
			{
				poo.MadeFumes-=f.OnExposedToPoo;
			}
		}
		//_timeHandler.NewHour+=f.OnNewHour;
	}

	private void Intro()
	{
		_lblEventInfo.QueueText("It's all up to you! Good luck!", 0.5f);
	}

	public override void _Process(float delta)
	{
		Vector2 worldPos = _inventoryPlayer.GetGlobalMousePosition();

		_lblTime.Text = _timeHandler.GetTime();
		
		_prSatisfaction.Value = GetSatisfaction();
		// ProcessFoods(delta);
		ProcessAllItems(delta);

		if (CheckWin())
		{
			WinScreen();
			// SetProcess(false);
		}
		if (CheckLose())
		{
			LoseScreen();
			// SetProcess(false);
		}
		// ProcessPoos(delta);

		if (Input.IsActionJustPressed("SetTile"))
		{
			if (_inventoryShop.Visible)
			{
				BuyItem(worldPos);
			}

			if (_inventoryPlayer.Visible)
			{
				UseItem(worldPos);
			}
			// InventoryItem item = (InventoryItem) _scnInventoryItem.Instance();
			// item.ItemType = InventoryItem.ItemClass.Turret;
			// item.Init((Turret)_scnTurret.Instance());
			// _inventoryGrid.AddItem(item);
			// GD.Print("position is: ", item.RectPosition);
			// Vector2 world = _inventoryGrid.GetGlobalMousePosition();
			// int[] coord = _inventoryGrid.WorldToGrid(world);
			// Vector2 wPos = _inventoryGrid.GridToWorld(coord);
		}
		if (Input.IsActionJustPressed("ClearTile"))
		{

			// GD.Print(_inventoryShop.GetNumberOfItems(InventoryItem.ItemClass.Toy));
			// InventoryItem item = _inventoryGrid.GetItemFromCoord(worldPos);
			// if (item != null)
			// {
			// 	_inventoryGrid.RemoveItem(item);
			// }
		}
	}

	private void CheckEventChance()
	{
		if (_timeHandler.GetHour() >= 10 && _timeHandler.GetHour() <= 19)
		{
			if (new Random().Next(0,20) >= 19)// 5% per hour.. ,40) >= 37) // 7.5% chance per hour
			{
				int randIndex = new Random().Next(0, _events.Count);
				_shaker.Shake();
				_events[randIndex]();
				DidScaryEvent?.Invoke();
			}
		}
	}


	private void EventHoover()
	{
		// AudioStream vaccumNarrative = null;

		PlayNarrativeLine(AudioData.GameSounds.Vacuum);
		// if (_narrativeLines.Contains(AudioData.GameSounds.Vacuum))
		// {
		// 	vaccumNarrative = _gameSounds[AudioData.GameSounds.Vacuum];
		// 	// AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.AttackInNight], AudioData.SoundBus.Voice);
		// 	_narrativeLines.Remove(AudioData.GameSounds.Vacuum);
		// }
		_lblEventInfo.QueueText("The foxes are SCARED!", 2, LblEventInfo.EventColor.Red);
		_pictureStory.Reset();
		// _pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", @"Oh no! Your neighbour has started hoovering and the foxes are very scared!
				
		// Quickly! Pet the foxes before it's too late!");
		_pictureStory.AddScreen("res://Stages/Game/StoryPicture/Hoover.png", @"The foxes are terrified! The neighbours are playing loud and tasteless music.");

		_pictureStory.Start();
		AdjustFoxesLevel(2, 20, 60);
	}

	private void EventFoodPoisoning()
	{
		PlayNarrativeLine(AudioData.GameSounds.FoodPoisoning);
		_lblEventInfo.QueueText("SICK foxes!", 2, LblEventInfo.EventColor.Red);
		// _lblEventInfo.QueueText("HOOVER EVENT", 6, LblEventInfo.EventColor.Red);
		_pictureStory.Reset();
		// _pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", @"Oh no! Your foxes had some bad take out!
				
		// Quickly! Give them some medicine before it's too late!");
		_pictureStory.AddScreen("res://Stages/Game/StoryPicture/Poisoning.png", @"The foxes look sick! They've been eating from the bins! They need medicine.");

		_pictureStory.Start();
		AdjustFoxesLevel(0, 20, 60);
	}

	private void EventFoodStolen()
	{
		// AudioStream foodStolenNarrative = null;
		PlayNarrativeLine(AudioData.GameSounds.FoodStolen);
		// if (_narrativeLines.Contains(AudioData.GameSounds.FoodStolen))
		// {
		// 	foodStolenNarrative = _gameSounds[AudioData.GameSounds.FoodStolen];
		// 	// AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.AttackInNight], AudioData.SoundBus.Voice);
		// 	_narrativeLines.Remove(AudioData.GameSounds.FoodStolen);
		// }
		_lblEventInfo.QueueText("HUNGRY foxes!", 2, LblEventInfo.EventColor.Red);
		// _lblEventInfo.QueueText("HOOVER EVENT", 6, LblEventInfo.EventColor.Red);
		_pictureStory.Reset();
		// _pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.AddScreen("res://Stages/Game/StoryPicture/FoodStolen.png", @"The foxes’ stomachs are rumbling! Their food stores have been ransacked!");

		_pictureStory.Start();
		AdjustFoxesLevel(1, 20, 60);
	}

	private void WinScreen()
	{
		// AudioStream winNarrative = _gameSounds[AudioData.GameSounds.Win];
		// PlayNarrativeLine(AudioData.GameSounds.Win, true);
		// if (_narrativeLines.Contains(AudioData.GameSounds.Win))
		// {
		// 	winNarrative = _gameSounds[AudioData.GameSounds.Win];
		// 	// AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.AttackInNight], AudioData.SoundBus.Voice);
		// 	_narrativeLines.Remove(AudioData.GameSounds.Win);
		// }
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnEndGame;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/Game/StoryPicture/Win.png", @"You have won!
		
		Pandora’s time to shine had come to an end. The family of four foxes had grown large and muscular - they were ready to take on their enemies, the fox hunters.", 
			_gameSounds[AudioData.GameSounds.Win]);
		_pictureStory.Start();

		// 		PictureStory pictureStory = (PictureStory) ((PackedScene)GD.Load("res://Common/Nodes/PictureStory/PictureStory.tscn")).Instance();//= new PictureStory();
		// GetNode("HUD").AddChild(pictureStory);
		// pictureStory.Reset();
		// pictureStory.PictureStoryFinished+=this.OnEndGame;
		// pictureStory.StayPausedOnFinish = true;
		// pictureStory.FadeOutOnFinish = false;
		// pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", @"WINNER WINNER");
		// pictureStory.Start();
	}

	private void LoseScreen()
	{
		// AudioStream loseNarrative = null;
		// PlayNarrativeLine(AudioData.GameSounds.LosePandora, true);
		// if (_narrativeLines.Contains(AudioData.GameSounds.LosePandora))
		// {
		// 	loseNarrative = _gameSounds[AudioData.GameSounds.LosePandora];
		// 	// AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.AttackInNight], AudioData.SoundBus.Voice);
		// 	_narrativeLines.Remove(AudioData.GameSounds.LosePandora);
		// }
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnEndGame;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/Game/StoryPicture/Lose.png", @"You have lost.
		
		Pandora found herself alone. The foxes left her to do what she does best... bash buttons and wait for payday.", _gameSounds[AudioData.GameSounds.LosePandora]);
		_pictureStory.Start();
		
	}

	private void OnEndGame()
	{
		// GD.Print("TEST TEST TEST TEST TEST END");
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}

	private void AdjustFoxesLevel(int level, int numLower, int numUpper) // 0 = health 1 = satiety 2 = happy
	{
		float num = new Random().Next(numLower, numUpper);
		foreach (Fox f in _foxes)
		{
			switch (level)
			{
				case 0:
					f.Health -= num;
					break;
				case 1:
					f.Satiety -= num;
					break;
				case 2:
					f.Happiness -= num;
					break;
			}
		}
	}

	// private float _pooConstant = 0.1f;

	// private void ProcessPoos(float delta)
	// {
	// 	foreach (Node n in _cntPoos.GetChildren())
	// 	{
	// 		if (n is Poo poo)
	// 		{
	// 			GD.Print(delta * _timeHandler.GetSpeed() * _pooConstant * new Random().Next(100,200));
	// 		}
	// 	}
	// }

	private float ProcessItem(float delta, List<InventoryItem> itemList, float foxLevel, float itemConstant, Action<InventoryItem> destroyMethod)
	{

		foreach (InventoryItem item in itemList.ToList())
		{
			// GD.Print("are we here");
			// GD.Print(item.Item.ItemName);
			if (foxLevel == 100)
			{
				continue;
			}
			item.Item.Health -= delta * _timeHandler.GetSpeed() * itemConstant;
			foxLevel += delta * _timeHandler.GetSpeed() * itemConstant;

			if (item.Item.Health <= 0)
			{
				_lblEventInfo.QueueText(string.Format("The {0} is used up!", item.Item.ItemName), 3);
				destroyMethod(item);
			}
		}
		return foxLevel;
	}

// to pet fox

	// private void OnFoxInputEvent(Viewport viewport, InputEvent ev, int shape_idx)
	// {
	// 	if (ev.IsPressed())
	// 	{
	// 		if (ev is InputEventMouseButton btn)
	// 		{
	// 			if (btn.ButtonIndex == (int)ButtonList.Left)
	// 			{
	//				if (_petCooldownTimer.IsStopped())
	//				{
	// 					FoxClicked?.Invoke();
	// 					Fox.Happiness+= new Random().Next(5,20);
	//					_petCooldownTimer.WaitTime = new Random().Next(30,100)/_currentSpeed;
	//					_petCooldownTimer.Start();
	//				}
	// 			}
	// 		}
	// 	}
	// }

	private void OnFoxPet()
	{
		GetToldOff();
		UpdateTimesheet(-(_payPerHour * _timeToPetFox/60));
		_lblEventInfo.QueueText("Teehee! Fox loves it when you play!", 3);
	}

	private void ProcessAllItems(float delta)
	{
		foreach (Fox f in _foxes)
		{
			// GD.Print("Fox happiness: ", f.Happiness);
			// GD.Print("Fox satiety: ", f.Satiety);
			f.Happiness = ProcessItem(delta, _toys, f.Happiness, _toyHealthConstant, DestroyToy);
			f.Satiety = ProcessItem(delta, _foods, f.Satiety, _foodHealthConstant, DestroyFood);
			f.Health = ProcessItem(delta, _medicines, f.Health, _medicineHealthConstant, DestroyMedicine);
		}
		// // GD.Print("Food count: ", _foods.Count);
		// foreach (InventoryItem item in _toys.ToList())
		// {
		// 	foreach (Fox f in _foxes)
		// 	{
		// 		// GD.Print("Fox satiety: ", f.Satiety);
		// 		if (f.Happiness == 100)
		// 		{
		// 			continue;
		// 		}
		// 		// float constant = new Random().Next(_maxConstantMultiplier)*_foodHealthConstant;
		// 		item.Item.Health -= delta * _timeHandler.GetSpeed() * _toyHealthConstant;
		// 		f.Happiness += delta * _timeHandler.GetSpeed() * _toyHealthConstant;

		// 	}

		// 	// GD.Print("Food health: ", item.Item.Health);
			

		// 	if (item.Item.Health <= 0)
		// 	{
		// 		DestroyToy(item);
		// 	}
		// }
	}

	private void UpdateTimesheet(int pay)
	{
		// GD.Print("Hour: ", _timeHandler.GetHour());
		if (_timeHandler.GetHour() >= 9 && _timeHandler.GetHour() <= 17)
		{
			_weeklyWage += pay;
			_lblNextMoney.Text = "Next pay: £" + _weeklyWage;
		}
		// GD.Print(_weeklyWage);
	}

	private void OnNewDay(string day)
	{
		_toldOff = false;
		// GD.Print("test");
		_lblDay.Text = _timeHandler.GetDay();

	}

	private void OnNewHour(int hour)
	{
		// _lblTime.Text = _timeHandler.GetTime();
		UpdateTimesheet(_payPerHour);
		DamageDefences();
		CheckEventChance();
		AdjustBrightness();

		if (hour == 22)
		{
			PlayNarrativeLine(AudioData.GameSounds.NightFell);
			// if (_narrativeLines.Contains(AudioData.GameSounds.NightFell))
			// {
			// 	AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.NightFell], AudioData.SoundBus.Voice);
			// 	_narrativeLines.Remove(AudioData.GameSounds.NightFell);
			// }
		}

		if (hour == 0 && _timeHandler.GetDay() == "Saturday")
		{
			PayDay();
			_lblNextMoney.Text = "Next pay: £" + _weeklyWage;
		}
	}

	private void AdjustBrightness()
	{
		Color c = _texGarden.Modulate;
		// c.v = 0.5f;
		// _texGarden.Modulate = c;
		if (_timeHandler.GetHour() >= 17 && _timeHandler.GetHour() <= 23)
		{
			c.v -= 0.1f;
		}

		if (_timeHandler.GetHour() >= 4 && _timeHandler.GetHour() <= 15)
		{
			c.v += 0.1f;
		}		
		c.v = Mathf.Clamp(c.v, 0.2f, 1f);
		_texGarden.Modulate = c;
	}

	private void DamageDefences()
	{
		if (_timeHandler.GetHour() < 8 || _timeHandler.GetHour() > 22)
		{
			if (new Random().Next(0,20) >= 19) // 5% chance per hour of killing a turret
			{
				DestroyRandomTurret();
			}
		}
	}

	private void PlayNarrativeLine(AudioData.GameSounds audio, bool interrupt = false)
	{
		if (_narrationPlayer.Playing && !interrupt)
		{
			return;
		}
		if (_narrativeLines.Contains(audio))
		{
			AudioHandler.PlaySound(_narrationPlayer, _gameSounds[audio], AudioData.SoundBus.Voice);
			_narrativeLines.Remove(audio);
		}
	}

	private void DestroyRandomTurret()
	{	
		
		// AudioHandler.PlaySound(_soundPlayer, _foxSounds[_foxWhines[whineIndex]], AudioData.SoundBus.Voice);
		// if (_narrativeLines.Contains(AudioData.GameSounds.AttackInNight) && !_narrationPlayer.Playing)
		// {
		// 	AudioHandler.PlaySound(_narrationPlayer, _gameSounds[AudioData.GameSounds.AttackInNight], AudioData.SoundBus.Voice);
		// 	_narrativeLines.Remove(AudioData.GameSounds.AttackInNight);
		// }
		
		PlayNarrativeLine(AudioData.GameSounds.AttackInNight);
		// if no turrets, hurt a fox
		if (_turrets.Count == 0)
		{
			_lblEventInfo.QueueText("Oh no! Enemies attack! There's nothing to stop them...", 1);
			HurtRandomFox(10,50);
			// GD.Print("OH NO - no defences = damaged fox!!!");
			return;
		}
		int randIndex = new Random().Next(0,_turrets.Count);
		InventoryItem turret = _turrets[randIndex];
		_lblEventInfo.QueueText("Oh no! Enemies attack! But they were deterred by your defences!", 1);
		DestroyTurret(turret); // if too hard, we can either reduce the chance of destroying the turret, or we can damage the turret for x health instead
	}
	
	// hurt a random fox health. This is temporary until sarah implements it in fox ***TEMPORARY**
	private void HurtRandomFox(int damageLower, int damageUpper)
	{
		if (_foxes.Count == 0)
		{
			return;
		}
		int index = new Random().Next(0,_foxes.Count);
		Fox f = _foxes[index % _foxes.Count];
		f.FoxHurt(damageLower, damageUpper); //.Health -= new Random().Next(damageLower,damageUpper);

		_shaker.Shake();
	}

	private void OnFoxDied(int foxID)
	{
		Fox f = _foxes.Find(x => x.foxID == foxID);
		if (f.IsFlipped())
		{
			f.Position = new Vector2(f.Position.x - 50, f.Position.y);
		}
		
		_foxPositions.Add(Tuple.Create<Vector2, bool>(f.Position, f.IsFlipped()));
		_lblEventInfo.QueueText("A fox has flown to the heavens! You must replace it!", 1);
		DisconnectFoxEvents(f);
		_foxes.Remove(f);
	}


	private void UseItem(Vector2 worldPos)
	{
		InventoryItem item = _inventoryPlayer.GetItemFromCoord(worldPos);
		if (item != null)
		{
			item.Item.Use();
		}
	}

	private void OnItemUsed(int itemID)
	{
		// InventoryItem item = GetPlayerItemByID(itemID);
		// _inventoryPlayer.RemoveItem(item);
		// switch (item.GetItemClass())
		// {
		// 	case InventoryItem.ItemClass.Turret:
		// 	{

		// 		break;
		// 	}
		// }
	}

	private InventoryItem GetPlayerItemByID(int itemID)
	{
		InventoryItem item = _inventoryPlayer.GetItemByID(itemID);
		if (item == null)
		{
			// GD.Print(" Item with ID ", itemID, "does not exist in the player's inventory!");
		}
		return item;
	}

	private void PayDay()
	{
		// if (_paidToday)
		// {
		// 	return;
		// }
		SetMoney(_money + _weeklyWage);
		_weeklyWage = 0;
		ShopRestock();
		// AudioHandler.PlaySound(_soundPlayer, _gameSounds[AudioData.GameSounds.Chaching], AudioData.SoundBus.Effects);
		_lblEventInfo.QueueText("It's payday! Time to go shopping...", 1);
		// _paidToday = true;
	}

	private void ShopRestock() 
	{
			List<InventoryItem.ItemClass> shopItemsToStock = _shopItems.ToList();

			while (_inventoryShop.GetItemList().Count < _inventoryShop.Capacity)
			{
				// make it so that foxes only restock if we have less than 4 foxes and limit stocked foxes to 4
				if (_foxes.Count >= 4 || _inventoryShop.GetNumberOfItems(InventoryItem.ItemClass.PresentFox) >= 4)
				{	
					shopItemsToStock.Remove(InventoryItem.ItemClass.PresentFox);
				}
				// and don't stock turrets if we have 4 or more on the board..and limit stocked turrets to 8
				if (_turrets.Count >= 4 || _inventoryShop.GetNumberOfItems(InventoryItem.ItemClass.Turret) >= 8)
				{
					shopItemsToStock.Remove(InventoryItem.ItemClass.Turret);
				}
				// and limit stocked medicines to 10
				if (_inventoryShop.GetNumberOfItems(InventoryItem.ItemClass.Medicine) >= 10)
				{
					shopItemsToStock.Remove(InventoryItem.ItemClass.Medicine);
				}
				// and limit stocked toys to 15
				if (_inventoryShop.GetNumberOfItems(InventoryItem.ItemClass.Toy) >= 15)
				{
					shopItemsToStock.Remove(InventoryItem.ItemClass.Toy);
				}
				int randIndex = new Random().Next(0, shopItemsToStock.Count);
				AddItemToShop(shopItemsToStock[randIndex]);
			}
	}


	private void AddItemToShop(InventoryItem.ItemClass itemClass)
	{
		InventoryItem item = MakeNewItem(itemClass);
		_inventoryShop.AddItem(item);
	}

	private InventoryItem MakeNewItem(InventoryItem.ItemClass itemClass)
	{
		InventoryItem newItem = (InventoryItem) _scnInventoryItem.Instance();
		newItem.ItemID = _itemUID;
		_itemUID += 1;
		// newItem.ItemUsed+=this.OnItemUsed;
		newItem.SetItemClass(itemClass);

		switch (itemClass)
		{
			case InventoryItem.ItemClass.Turret:
				newItem.Init((Turret)_scnTurret.Instance());
				((Turret)newItem.Item).TurretUsed+=this.OnTurretUsed;
				break;
			case InventoryItem.ItemClass.Toy:
				newItem.Init((Toy)_scnToy.Instance());
				((Toy)newItem.Item).ToyUsed+=this.OnToyUsed;
				break;
			case InventoryItem.ItemClass.Food:
				newItem.Init((Food)_scnFood.Instance());
				((Food)newItem.Item).FoodUsed+=this.OnFoodUsed;
				break;
			case InventoryItem.ItemClass.PresentFox:
				newItem.Init((PresentFox)_scnPresentFox.Instance());
				((PresentFox)newItem.Item).PresentFoxUsed+=this.OnPresentFoxUsed;
				break;
			case InventoryItem.ItemClass.Medicine:
				newItem.Init((Medicine)_scnMedicine.Instance());
				((Medicine)newItem.Item).MedicineUsed+=this.OnMedicineUsed;
				break;
		}

		newItem.Item.Init(newItem);
		newItem.Item.ItemUsed+=this.OnItemUsed;

		return newItem;
	}
	

	private void OnToyUsed(int itemID)
	{
		InventoryItem item = GetPlayerItemByID(itemID);
		if (PlaceItemOnGarden(item, _cntToys, _toyPositions))
		{
			_lblEventInfo.QueueText("You throw a toy into the garden. Teehee!",3);
			_toys.Add(item);
		}
		// if (_toyPositions.ToList().Count >= 1)
		// {
		// 	_inventoryPlayer.RemoveItem(item);
		// 	_cntTurrets.AddChild(item);
		// 	item.RectPosition = _toyPositions[0];
		// 	_toyPositions.Remove(_toyPositions[0]);
		// }
		// else
		// {
		// 	GD.Print("Error, cannot use ", item.Item.ItemName, " as already reached maximum capacity");
		// }
		// GD.Print("MAKE FOXES HAPPY");
	}
	private void OnFoodUsed(int itemID)
	{
		InventoryItem item = GetPlayerItemByID(itemID);
		if (PlaceItemOnGarden(item, _cntFood, _foodPositions))
		{
			_foods.Add(item);
			_lblEventInfo.QueueText("Yum! A fed fox is a healthy fox!",3);
		}

		// if (_toyPositions.ToList().Count >= 1)
		// {
		// 	_inventoryPlayer.RemoveItem(item);
		// 	_cntTurrets.AddChild(item);
		// 	item.RectPosition = _toyPositions[0];
		// 	_toyPositions.Remove(_toyPositions[0]);
		// }
		// else
		// {
		// 	GD.Print("Error, cannot use ", item.Item.ItemName, " as already reached maximum capacity");
		// }
		// GD.Print("MAKE FOXES HAPPY");
	}

	private void OnMedicineUsed(int itemID)
	{
		InventoryItem item = GetPlayerItemByID(itemID);
		if (PlaceItemOnGarden(item, _cntMedicine, _medicinePositions))
		{
			_medicines.Add(item);
			_lblEventInfo.QueueText("Live long and prosper!",3);
		}
	}

	private void OnPresentFoxUsed(int itemID)
	{
		InventoryItem item = GetPlayerItemByID(itemID);
		if (_foxPositions.ToList().Count >= 1)
		{
			_inventoryPlayer.RemoveItem(item);
			AddFox(_foxPositions[0]);
			// _cntFoxes.AddChild(item);
			// item.RectPosition = itemPositions[0];
			_foxPositions.Remove(_foxPositions[0]);
			_lblEventInfo.QueueText("A present! I wonder what's inside!",2);
		}
		else
		{
			_lblEventInfo.QueueText(string.Format("It's a little crowded to use that {0}!", item.Item.ItemName),3);
			// GD.Print("Error, cannot use ", item.Item.ItemName, " as already reached maximum capacity");
		}
	}

	private bool PlaceItemOnGarden(InventoryItem item, Control itemContainer, List<Vector2> itemPositions)
	{

		if (itemPositions.ToList().Count >= 1)
		{
			_inventoryPlayer.RemoveItem(item);
			itemContainer.AddChild(item);
			item.RectPosition = itemPositions[0];
			itemPositions.Remove(itemPositions[0]);
			// item.RectScale = new Vector2(2,2);
			return true;
		}
		else
		{
			_lblEventInfo.QueueText(string.Format("You can't use that {0}! There are too many already!", item.Item.ItemName),3);
			// GD.Print("Error, cannot use ", item.Item.ItemName, " as already reached maximum capacity");
			return false;
		}
	}

	private void DestroyTurret(InventoryItem item)
	{
		_turretPositions.Add(item.RectPosition);
		_turrets.Remove(item);
		item.Die();
	}
	private void DestroyToy(InventoryItem item)
	{
		_toyPositions.Add(item.RectPosition);
		_toys.Remove(item);
		item.Die();
	}
	private void DestroyFood(InventoryItem item)
	{
		_foodPositions.Add(item.RectPosition);
		_foods.Remove(item);
		item.Die();
	}
	private void DestroyMedicine(InventoryItem item)
	{
		_medicinePositions.Add(item.RectPosition);
		_medicines.Remove(item);
		item.Die();
	}

	private void OnTurretUsed(int itemID)
	{
		InventoryItem item = GetPlayerItemByID(itemID);
		// if (_turrets.Count < 4)
		// {
		// 	_turrets.Add(item);
		// 	// _inventoryPlayer.RemoveItem(item);
		// }

		if (_turretPositions.ToList().Count >= 1)
		{
			_inventoryPlayer.RemoveItem(item);
			_cntTurrets.AddChild(item);
			item.RectPosition = _turretPositions[0];
			item.RectScale = new Vector2(1.5f,1.5f);
			if (item.RectPosition.y > 200)
			{
				item.RectScale = new Vector2(2f, 2f);
			}
			_turretPositions.Remove(_turretPositions[0]);
			_turrets.Add(item);
			_lblEventInfo.QueueText("A safe fox is a healthy fox!",3);
		}
		else
		{
			_lblEventInfo.QueueText(string.Format("You can't use that {0}! There are too many already!", item.Item.ItemName),3);
		}

	}

	private void BuyItem(Vector2 worldPos)
	{
		InventoryItem item = _inventoryShop.GetItemFromCoord(worldPos);
		if (item != null)
		{
			if (CanAffordItem(item))
			{
				if (_inventoryPlayer.GetItemList().Count < _inventoryPlayer.Capacity)
				{
					_inventoryShop.RemoveItem(item);
					_inventoryPlayer.AddItem(item);
					SetMoney(_money - item.Cost);
					_lblEventInfo.QueueText(string.Format("You buy a brand new {0}!", item.Item.ItemName),3);
				}
				else
				{
					_lblEventInfo.QueueText(string.Format("Your bags are full!", item.Item.ItemName),3);
				}
			}
			else
			{
				_lblEventInfo.QueueText(string.Format("You can't afford that {0}!", item.Item.ItemName),3);
			}
		}
	}
	
	private bool CanAffordItem(InventoryItem item)
	{
		if (_money >= item.Cost)
		{
			return true;
		}
		return false;
	}

	// if you spend time during working hours doing something, wages get docked!
	// private void SpendTime(int hours) // duplicated somewhere else.. i forgot i wrote this method
	// {
	// 	if (_timeHandler.GetHour() >= 9 && _timeHandler.GetHour() <= 17)
	// 	{
	// 		if (_timeHandler.GetHour() < 13 && _timeHandler.GetHour() >= 14)
	// 		{
	// 			_weeklyWage -= _payPerHour * hours;
	// 		}
	// 	}
	// }

	private void SetMoney(int newMoney)
	{
		_money = newMoney;
		_lblMoney.Text = "Money: £" + newMoney;
	}

	private void OnBtnInventoryPressed()
	{
		_inventoryPlayer.Show();
		_inventoryShop.Hide();
		DisableInvButtons();
	}

	// private void OnBtnInventoryHidePressed()
	// {
	// 	_inventoryPlayer.Hide();
	// 	_inventoryShop.Hide();
	// }

	private void DisableInvButtons()
	{
		GetNode<BaseButton>("HUD/BtnInventory").Disabled = true;
		GetNode<BaseButton>("HUD/BtnShop").Disabled = true;
	}

	private void OnInventoryClosed(int UID)
	{
		// GD.Print("TEST");
		GetNode<BaseButton>("HUD/BtnInventory").Disabled = false;
		GetNode<BaseButton>("HUD/BtnShop").Disabled = false;
	}
	
	private void OnBtnShopPressed()
	{
		_inventoryShop.Show();
		_inventoryPlayer.Hide();
		DisableInvButtons();
	}

	private void OnBtnPausedPress()
	{
		_timeHandler.SetSpeed(TimeHandler.SpeedSetting.Paused);
		GetNode<BtnBase>("HUD/BtnPause").Disabled = true;
		GetNode<BtnBase>("HUD/BtnSpeedNormal").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedDouble").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedQuadruple").Disabled = false;
	}


	private void OnBtnSpeedNormalPressed()
	{
		_timeHandler.SetSpeed(TimeHandler.SpeedSetting.Normal);
		GetNode<BtnBase>("HUD/BtnPause").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedNormal").Disabled = true;
		GetNode<BtnBase>("HUD/BtnSpeedDouble").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedQuadruple").Disabled = false;
	}


	private void OnBtnSpeedDoublePressed()
	{
		_timeHandler.SetSpeed(TimeHandler.SpeedSetting.Double);
		GetNode<BtnBase>("HUD/BtnPause").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedNormal").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedDouble").Disabled = true;
		GetNode<BtnBase>("HUD/BtnSpeedQuadruple").Disabled = false;
	}


	private void OnBtnSpeedQuadruplePressed()
	{
		_timeHandler.SetSpeed(TimeHandler.SpeedSetting.Quadruple);
		GetNode<BtnBase>("HUD/BtnPause").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedNormal").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedDouble").Disabled = false;
		GetNode<BtnBase>("HUD/BtnSpeedQuadruple").Disabled = true;
	}

}
