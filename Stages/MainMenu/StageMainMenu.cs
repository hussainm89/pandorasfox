// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private Dictionary<AudioData.MainMenuSounds, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSounds>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private TextureRect _popAbout;
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		_popAbout = GetNode<TextureRect>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSounds.Music], AudioData.SoundBus.Music);
		_popAbout.Visible = false;
		// Load settings at start if they exist. This should run at startup so if another scene is set at startup put this there.
		// GameSettings.Instance.LoadFromJSON();

	}
	private void Intro()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "One sunny Saturday morning, you respond to a curious scratching on your front door...", _mainMenuSounds[AudioData.MainMenuSounds.Test]);
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", @"What's this? A family of foxes? They stare at you desperately.
		
		// They are cold and shivering, and the pups are barely alive!
		
		// // Sympathising with their plight, you lead them into your garden. Silently, you promise to get them back on their paws, ready to face the hardships of the world...", _mainMenuSounds[AudioData.MainMenuSounds.Test]);
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Pandora’s life was SO predictable. She worked and slept and worked again...  there wasn’t an adventure in sight until one sunny Saturday morning that would change her dreary life forever...", _mainMenuSounds[AudioData.MainMenuSounds.Intro1]);
		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Pandora heard a curious scratching at her front door. There was a family of foxes on her doorstep pleading for help; cold, hungry, and hiding from fox-hunters, their lives were even more wretched than hers!", _mainMenuSounds[AudioData.MainMenuSounds.Intro2]);

		// _pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Objective: Look after the foxes by playing with them and buying supplies from the shop. These will appear in your inventory. When the foxes are happy and well, they will grow up. Raise four adult foxes to win the game.");


		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Pandora’s life was so predictable... until one day, on a sunny Saturday morning, a knock on the door would change her dreary life forever...", _mainMenuSounds[AudioData.MainMenuSounds.Intro1]);
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Pandora heard a curious scratching at her front door. On the doorstep, there was a family of foxes, cold, hungry, and hiding from fox hunters.", _mainMenuSounds[AudioData.MainMenuSounds.Intro2]);
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro.png", "Objective: Look after the foxes by playing with them and buying supplies from the shop. These will appear in your inventory. When the foxes are happy and well, they will grow up. Raise four adult foxes to win the game.");

//
		// _pictureStory.AddScreenVideo("res://1280.ogv");
		// _musicPlayer.Stop();
//res://1280.ogv
		_pictureStory.Start();
	}

	private void OnIntroFinished()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.Game);
	}

	private void OnBtnPlayPressed()
	{
		Intro();
	}

// 	private void ShowIntroStory()
// 	{
// 		_pictureStory.Reset();
// 		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
// 		_pictureStory.FadeOutOnFinish = false;
// 		string introStory = @"TEN YEARS AGO...
		
// 		You had it all: vaults of gold, a court full of friends and a home - nay! A castle! That was until Finnegan, a flame haired leprechaun, intruded into your dining room.
// “Feed me a meal and I’ll grant you a wish” he said, leaping onto your plate.

// Laughing, you tore a piece of buttered bread for the insignificant creature and replied, “Look around - I need nothing! So I wish for nothing!”

// The next morning you found yourself alone and friendless, with no gold in your purse, and only your raggedy shirt to call your own.

// You could think of nothing but revenge... until finally, you found the door 
// to Finnegan's tiny world.";
// 		_pictureStory.AddScreen("res://Stages/MainMenu/About3.png", introStory);
// 		_pictureStory.Start();	
// 	}

// 	private void OnIntroFinished()
// 	{
// 		SceneManager.SimpleChangeScene(SceneData.Stage.World);
// 	}

	private void OnBtnAboutPressed()
	{
		GetNode<TextureRect>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntHowToPlay").Visible = false;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnHowToPressed()
	{
		GetNode<TextureRect>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntHowToPlay").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = false;
	}

	private void OnBtnBackPressed()
	{
		GetNode<TextureRect>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				GD.Print("CLICKED OUTSIDE ABOUT");
				OnBtnBackPressed();
			}
		}
	}

	// private void _on_BtnLocalGame_pressed()
	// {   
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Game);  
	// 	// SceneManager.AnimatedChangeScene(SceneData.Stage.Game);   
	// 	// SceneManager.SimpleChangeScene(SceneData.Stage.Game, new Dictionary<string, object>() {
	// 	// 		{"gameConnectedState", StageGame.Connected.Local}
	// 	// 	 });
	// }

	// private void 


	// private void _on_BtnNetworkedGame_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Multiplayer);
	// }
	
	// private void _on_BtnScores_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Scores);
	// }

	// private void _on_BtnOptions_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Options);
	// }

	// private void OnBtnOnePlayerPressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
	// 			{"GameMode", 1}
	// 		 });
	// }


	// private void OnBtnTwoPlayersPressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
	// 			{"GameMode", 2}
	// 		 });
	// }


	private void _on_BtnQuit_pressed()
	{
		// ToggleMute();
		// OS.WindowFullscreen = false;
		//OS.window_fullscreen = !OS.window_fullscreen
		// SceneManager.AnimatedChangeScene(SceneData.Stage.IntensiveTest, persist:true, sharedData:new Dictionary<string, object>() {{"ID", 1}});
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = "Unmute";
		if (muted)
		{
			label = "Mute";
		}
		GetNode<Label>("BtnToggleMute/Label").Text = label;
	}
}
