// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};


	public enum MainMenuSounds {
		Music,
		Test,
		Intro1,
		Intro2
	};

	public enum GameSounds {
		Music,
		Chaching,
		BackToWork,
		AttackInNight,
		FoodStolen,
		LosePandora,
		NightFell,
		Vacuum,
		Win,
		FoodPoisoning
	};

	public enum FoxSounds {
		Laugh1,
		Laugh2,
		Laugh3,
		Laugh4,
		Whine1,
		Whine2,
		Whine3,
		Hungry1,
		Hungry2
	};


	public enum TestSounds {
		Voice,
		Effects,
		Music
	};

	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Common/UI/Buttons/BtnBase/ClickStart.ogg"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/FoxMenu.ogg"},
		{MainMenuSounds.Test, "res://Stages/Game/Sound/chaching.wav"},
		{MainMenuSounds.Intro1, "res://Stages/Game/Sound/Narration/PandoraAudio1.wav"},
		{MainMenuSounds.Intro2, "res://Stages/Game/Sound/Narration/PandoraAudio2.wav"},
	};

	public static Dictionary<FoxSounds, string> FoxSoundPaths = new Dictionary<FoxSounds, string>()
	{
		{FoxSounds.Laugh1, "res://Entities/Fox/Sounds/laugh1.wav"},
		{FoxSounds.Laugh2, "res://Entities/Fox/Sounds/laugh2.wav"},
		{FoxSounds.Laugh3, "res://Entities/Fox/Sounds/laugh3.wav"},
		{FoxSounds.Laugh4, "res://Entities/Fox/Sounds/laugh4.wav"},
		{FoxSounds.Whine1, "res://Entities/Fox/Sounds/whine1.wav"},
		{FoxSounds.Whine2, "res://Entities/Fox/Sounds/whine2.wav"},
		{FoxSounds.Whine3, "res://Entities/Fox/Sounds/whine3.wav"},
		{FoxSounds.Hungry1, "res://Entities/Fox/Sounds/hungry1.wav"},
		{FoxSounds.Hungry2, "res://Entities/Fox/Sounds/hungry2.wav"}
	};

	public static Dictionary<GameSounds, string> GameSoundPaths = new Dictionary<GameSounds, string>()
	{
		{GameSounds.Music, "res://Stages/Game/Sound/FoxGarden.ogg"},
		{GameSounds.Chaching, "res://Stages/Game/Sound/chaching.wav"},
		{GameSounds.BackToWork, "res://Stages/Game/Sound/backtowork.wav"},
		{GameSounds.AttackInNight, "res://Stages/Game/Sound/Narration/AttackinNight.wav"},
		{GameSounds.FoodStolen, "res://Stages/Game/Sound/Narration/FoodStolen.wav"},
		{GameSounds.LosePandora, "res://Stages/Game/Sound/Narration/LosePandora.wav"},
		{GameSounds.NightFell, "res://Stages/Game/Sound/Narration/NightFell3.wav"},
		{GameSounds.Vacuum, "res://Stages/Game/Sound/Narration/VacuumPandora.wav"},
		{GameSounds.Win, "res://Stages/Game/Sound/Narration/WinAudio.wav"},
		{GameSounds.FoodPoisoning, "res://Stages/Game/Sound/Narration/PandoraFoxSickEvent.wav"}
		

	};

		// AttackInNight,
		// FoodStolen,
		// LosePandora,
		// NightFell,
		// Vacuum,
		// Win
	public static Dictionary<TestSounds, string> TestSoundPaths = new Dictionary<TestSounds, string>()
	{
		{TestSounds.Voice, "res://Stages/Options/Sound/Voice.wav"},
		{TestSounds.Effects, "res://Stages/Options/Sound/Effects.wav"},
		{TestSounds.Music, "res://Stages/Options/Sound/Music.wav"}
	};


	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
