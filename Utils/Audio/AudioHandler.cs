// Audio handler script - handles playing sound via audiostreamplayers
using Godot;
using System;

public static class AudioHandler
{
	// Put 3D specific code here
	public static void PlaySound(AudioStreamPlayer3D player, AudioStream stream, AudioData.SoundBus bus)
	{
		player.Bus = AudioData.SoundBusStrings[bus];
		player.Stream = stream;
		player.Play();
	}

	// Put 2D specific code here
	public static void PlaySound(AudioStreamPlayer2D player, AudioStream stream, AudioData.SoundBus bus)
	{
		player.Bus = AudioData.SoundBusStrings[bus];
		player.Stream = stream;
		player.Play();
	}

	// Put general sound player code here
	public static void PlaySound(AudioStreamPlayer player, AudioStream stream, AudioData.SoundBus bus)
	{
		player.Bus = AudioData.SoundBusStrings[bus];
		player.Stream = stream;
		player.Play();
	}
}
