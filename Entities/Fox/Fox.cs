//TODO: Add present/hatching anim
//TODO: Need to add present anims. Set the texture to adult anims?
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class Fox : Area2D
{
	public float Health {get; set;}
	public float Happiness {get; set;}
	public float Satiety {get; set;}
	private int _currentSpeed;
	public float _constSatiety;
	public float _constHappiness;
	public float _constHealth;
	public float _pooExposure;
	//Events:
	public delegate void WantHealthDelegate();
	public event WantHealthDelegate WantHealth;
	public delegate void NeedHealthDelegate();
	public event NeedHealthDelegate NeedHealth;
	public delegate void WantHappinessDelegate();
	public event WantHappinessDelegate WantHappiness;
	public delegate void NeedHappinessDelegate();
	public event NeedHappinessDelegate NeedHappiness;
	public delegate void WantFoodDelegate();
	public event WantFoodDelegate WantFood;
	public delegate void FoxDidPooDelegate();
	public event FoxDidPooDelegate FoxDidPoo;
	public delegate void OnPetFoxDelegate();
	public event OnPetFoxDelegate PetFox;
	public delegate void NeedFoodDelegate();
	public event NeedFoodDelegate NeedFood;
	public delegate void OnFoxDiedDelegate(int foxID);
	public event OnFoxDiedDelegate OnFoxDied;

	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.FoxSounds, AudioStream> _foxSounds = AudioData.LoadedSounds<AudioData.FoxSounds>(AudioData.FoxSoundPaths);

	//bools
	bool wantFoodBool = false;
	bool needFoodBool = false;
	bool wantHappyBool = false;
	bool needHappyBool = false;
	bool wantHealthBool = false;
	bool needHealthBool = false;
	bool awake;
	//Sprites etc
	//Sprite wantHealthSprite;
	//Sprite needHealthSprite;
	//Sprite wantFoodSprite;
	//Sprite needFoodSprite;
	//Sprite wantHappySprite;
	//Sprite needHappySprite;
	Sprite foxSprite;
	Vector2 halfScale = new Vector2 ((float)1,(float)1);
	Vector2 fullScale = new Vector2 ((float)2,(float)2);
	Vector2 quarterScale = new Vector2 ((float)0.7,(float)0.7);
	Texture texture;
	// [Export]
	State state;
	AnimationPlayer anim;
	string currentAnim;
	int hourOfPoo;
	public string animation;
	Timer timer;
	bool levelUp;
	private int clickRadius = 100;
	bool mouseEntered;
	public int foxID {get; set;} 
	// [Export]
	bool flip; //Set the state and the flip via the exported variable
	CollisionShape2D collisionShape2D;
	private bool _foxDidSickPoo = false;
	private bool _foxDead = false;
	private string[] _sleepingAnims = new string[3] { "Sleeping", "Sleeping2", "Sleeping3"};
	private bool _levelledUpToday = true;

	public enum State
	{
		Baby,
		Pup,
		Adult
	}

	//When parameters reach 100, allow the fox to grow up
	private void SetState()
	{
		// GD.Print(" Satiety: ", Satiety);
		// GD.Print(" Health: ", Health);
		// GD.Print(" Happiness: ", Happiness);
		// only level up max once per day
		if (_levelledUpToday)
		{
			return;
		}
		if (Satiety >= 90 && Health >= 90 && Happiness >= 90) 
		{
			levelUp = true;
			_levelledUpToday = true;

		  
		}
		//baby grows up to pup (set a fox texture and half the scale)
		if (state == State.Baby && levelUp) 
		{
			Health = 55;
			Happiness = 55;
			Satiety = 55;
			levelUp = false;
			SetMaturity(State.Pup);
			// state = State.Pup;
			
			// foxSprite.SetScale(halfScale);
		}
		//pup grows up to adult - sprite is set to full scale
		if (state == State.Pup && levelUp)
		{
			Health = 55;
			Happiness = 55;
			Satiety = 55;
			levelUp = false;
			SetMaturity(State.Adult);
			// state = State.Adult;
			// foxSprite.SetScale(fullScale);
		}
	}

	public State GetMaturity()
	{
		return state;
	}

	public void SetMaturity(State foxState)
	{
		// foxState = state;
		state = foxState;
		if (state == State.Baby)
		{
			animation = "Baby";//Set the texture/scale
			// foxSprite.SetTexture(texture);
			this.Scale = quarterScale;
			// foxSprite.SetScale(quarterScale);
			anim.Play("Baby");
		}
		if (state == State.Pup)
		{
			animation = "Idle";
			// foxSprite.SetScale(halfScale);
			this.Scale = halfScale;
			anim.Play(animation);
		}
		if (state == State.Adult)
		{
			animation = "Idle";
			// foxSprite.SetScale(fullScale);
			this.Scale = fullScale;
			anim.Play(animation);
		}
	  //  fox.SetMaturity(Fox.State.Adult); In this SetMaturity method, need to set the texture and scale.
	}

	public override void _Ready()
	{
		_constSatiety = new Random().Next(5,10)*0.005f;
		_constHappiness = new Random().Next(5,10)*0.005f;
		_constHealth = new Random().Next(5,10)*0.005f;
		Health = 55;
		Happiness = 55;
		Satiety = 55;
		DecideRandomPooTime();
		OnSpeedChanged(2); 
		InstanceSprites();
		//HideEmotes();
		awake = true;
		anim = (AnimationPlayer)GetNode("Anim");
		timer = (Timer)GetNode("Timer");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		SetFlip(flip);
		// SetMaturity(State.Baby);
		collisionShape2D.Position = foxSprite.Position;
	}

	public void SetFlip(bool flip)
	{
		if (flip)
		{
			foxSprite.FlipH = flip;
			Vector2 offset = new Vector2 (100,0);
			Position = new Vector2(Position.x + 50, Position.y);
			//foxSprite.Offset = offset;
			GetNode<TextureRect>("TextureRect").RectPosition = new Vector2(GetNode<TextureRect>("TextureRect").RectPosition.x - 35, GetNode<TextureRect>("TextureRect").RectPosition.y);
			GetNode<Area2D>("Area2D").Position = new Vector2(GetNode<Area2D>("Area2D").Position.x - 35, GetNode<Area2D>("Area2D").Position.y);
		}
	}

	public bool IsFlipped()
	{
		return foxSprite.FlipH;
	}

	private void InstanceSprites()
	{
		//wantHealthSprite = (Sprite)GetNode("WantHealthEmote");
		//needHealthSprite = (Sprite)GetNode("NeedHealthEmote");
		//wantFoodSprite = (Sprite)GetNode("WantFoodEmote");
		//needFoodSprite = (Sprite)GetNode("NeedFoodEmote");
		//wantHappySprite = (Sprite)GetNode("WantHappyEmote");
		//needHappySprite = (Sprite)GetNode("NeedHappyEmote");
		foxSprite = (Sprite)GetNode("FoxSprite");
		collisionShape2D = (CollisionShape2D)GetNode("CollisionShape2D");
		texture = (Texture)ResourceLoader.Load("res://Entities/Fox/Textures/Hatching foxy.png"); //change this to the fox.
		
	}
/* 
	private void HideEmotes()
	{
		wantHealthSprite.Hide();
		needHealthSprite.Hide();
		wantFoodSprite.Hide();
		needFoodSprite.Hide();
		wantHappySprite.Hide();
		needHappySprite.Hide();
	} */

	public void OnSpeedChanged(int speed)
	{
		_currentSpeed = speed;
	}

	public override void _PhysicsProcess(float delta)
	{
		Awake(delta); 
		HealthDrain(delta); //should this be here or when it is awake only??
		SetState();
		//SetAnim();	
		// GD.Print("Current maturity for fox " + this.foxID + " : " + state.ToString());
	}

	public void HealthDrain(float delta) //health only drains when hungry
	{
		if (Satiety<1)
		{
			Health -= delta * _currentSpeed * _constHealth;
			Health = Mathf.Clamp(Health, 0, 100);
			HealthLevelCheck(Health);	
		}
		if (Satiety>50)
		{
			Health += delta * _currentSpeed * _constHealth;
			Health = Mathf.Clamp(Health, 0, 100);
			HealthLevelCheck(Health);
		}
	}

	public void HappinessLevelCheck(float happiness)
	{
		if (happiness>50)
		{
		//	needHappySprite.Hide();
		//	wantHappySprite.Hide();
			wantHappyBool = false;
			needHappyBool = false;
		}
		if (happiness<50 && happiness>10)
		{
		//	needHappySprite.Hide();
		//	wantHappySprite.Show();
			OnWantHappiness();
		}
		if (happiness<10)
		{	
		//	wantHappySprite.Hide();
		//	needHappySprite.Show();
			OnNeedHappiness();
		}
	}

	public void HealthLevelCheck(float health) //this just manages the health emotes
	{
		if (_foxDead)
			return;

		if (health>50)
		{
		//	needHealthSprite.Hide();
		//	wantHealthSprite.Hide();
			wantHealthBool = false;
			needHealthBool = false;
			_foxDidSickPoo = false;
		}
		if (health<50 && health>10)
		{
		//	needHealthSprite.Hide();
		//	wantHealthSprite.Show();
			OnWantHealth();
			_foxDidSickPoo = false;
		}
		if (health<10)
		{
		//	wantHealthSprite.Hide();
		//	needHealthSprite.Show();
			OnNeedHealth();
			if (!_foxDidSickPoo)
			{
				FoxDidPoo?.Invoke();
			} //make fox poo when sick
			_foxDidSickPoo = true;
		}
		if (health<1)
		{
			SetPhysicsProcess(false);
			SetProcessInput(false);
			FoxDied(foxID);
		}
	}
	public async void FoxDied(int foxID)
	{
		_foxDead = true;
		animation = "Die";
		//HideEmotes();
		anim.Play(animation);
		OnFoxDied?.Invoke(this.foxID);	
		WantHealth = null;
		NeedHealth = null;
		WantHappiness = null;
		NeedHappiness = null;
		WantFood = null;
		FoxDidPoo = null;
		PetFox = null;
		NeedFood = null;
		OnFoxDied = null;

		await ToSignal(anim, "animation_finished");
		OnFoxDiedAnimationFinished();			
	}

	// [Signal]
	// public delegate void MySignal();

	// public async void MyAsyncMethod()
	// {
	// 	GD.Print("Hello");
	// 	/// do something
	// 	await ToSignal(this, nameof(MySignal));
	// 	GD.Print("Goodbye");
	// }

	// public void AsyncWaitForThis()
	// {
	// 	EmitSignal(nameof(MySignal));
	// }

	private void OnFoxDiedAnimationFinished()
	{
		Visible = false;
		QueueFree();
	}

	public void SatietyLevelCheck(float satiety)
	{
		if (satiety>50)
		{
		//	needFoodSprite.Hide();
		//	wantFoodSprite.Hide();
			wantFoodBool = false;
			needFoodBool = false;
		}
		if (satiety<50 && satiety>10)
		{
		//	needFoodSprite.Hide();
		//	wantFoodSprite.Show();
			wantFoodBool = true;
			OnWantFood();
		}
		if (satiety<10)
		{
		//	wantFoodSprite.Hide();
		//	needFoodSprite.Show();
			OnNeedFood();
		}
	}

	public void OnWantHealth()
	{
		WantHealth?.Invoke();
	}
	public void OnNeedHealth()
	{
		NeedHealth?.Invoke();
	}
	public void OnWantHappiness()
	{
		WantHappiness?.Invoke();
	}
	public void OnNeedHappiness()
	{
		NeedHappiness?.Invoke();
	}
	public void OnWantFood()
	{
		WantFood?.Invoke();
	}
	public void OnNeedFood()
	{
		NeedFood?.Invoke();
	}


	private void OnButtonPressed() //for testing
	{
		OnNewHour(5); 
		Satiety -= 5;
		Happiness -= 5;
		Health -= 5; 
		// GD.Print(Satiety, Happiness, Health);
	}

	public void FoxHurt(int damageLower, int damageUpper)
	{
		int whineIndex = new Random().Next(0, _foxWhines.Count);
		AudioHandler.PlaySound(_soundPlayer, _foxSounds[_foxWhines[whineIndex]], AudioData.SoundBus.Voice);
		Health -= new Random().Next(damageLower, damageUpper);
		animation = "Hurt";
		anim.Play(animation);
		HealthLevelCheck(Health);
		// Timer timer = new Timer();
		// timer.OneShot = true;
		// timer.Autostart = false;
		// AddChild(timer);
		// timer.WaitTime = 0.1f;
		// timer.Connect("timeout", this, nameof(TempTimeout));
		// Modulate = new Color(10,10,10,10);
		// timer.Start();
		
	}
	
	public void FlashFox()
	{
		Modulate = new Color(10,10,10,10);
	}
	public void UnflashFox()
	{
		Modulate = new Color(1,1,1,1);
	}

	public void MakeFoxHappy(int happyLower, int happyUpper)
	{
		Happiness += new Random().Next(happyLower, happyUpper);
	}
	
	public void OnNewDay(string day)
	{
		// GD.Print("ON NEW DAY CALLED");
		// _levelledUpToday = false;
	}

	public void OnNewHour(int hour)
	{
		int wakeUpTime = new Random().Next(6,10);
		int sleepTime = new Random().Next(20,23);
		if (hour<wakeUpTime|| hour > sleepTime)
		{
			awake = false;
			//Sleep();
		}
		if (hour>wakeUpTime && hour <sleepTime)
		{
			awake = true;
		}
		if (hour == 4) //at 4am every day, randomly decide which hour fox will do poo. also fox can level up just once per day (from the 2nd day)
		{
			_levelledUpToday = false;
			DecideRandomPooTime(); //1 poo a day
		}
		if (hour == hourOfPoo)
		{
			FoxDidPoo?.Invoke();
		}		
	}

	public void DecideRandomPooTime()
	{
		hourOfPoo = new Random().Next(7,20);
		// GD.Print("hour of poo" + hourOfPoo);		
	}

	public void Sleep()
	{
		//PlayAnim("Sleeping");
	}

	public void Awake(float delta) //fox able to get hungry, //fox able to get sad
	{	
		if (awake)
		{
			FoxGetsHungry(delta);
			FoxGetsSad(delta);
		}	
	}

	public void FoxGetsHungry(float delta)
	{
		Satiety -= delta * _currentSpeed * _constSatiety;
		Satiety = Mathf.Clamp(Satiety, 0, 100);
		SatietyLevelCheck(Satiety);
	}

	public void FoxGetsSad(float delta)
	{
		Happiness -= delta * _currentSpeed * _constHappiness;
		Happiness = Mathf.Clamp(Happiness, 0, 100);
		HappinessLevelCheck(Happiness);
	}

	public void OnExposedToPoo() //what calls this? World or fox?. I CALL IT
	{
		//	_pooExposure = new Random().Next(10,30);
			//Health -= _pooExposure;
			FoxHurt(10,30);
			Health = Mathf.Clamp(Health, 0, 100);
			HealthLevelCheck(Health);
			//Show sick emote?
			// GD.Print("Fox was exposed to poo");
	}

	public void OnScaryEvent()
	{
		int whineIndex = new Random().Next(0, _foxWhines.Count);
		AudioHandler.PlaySound(_soundPlayer, _foxSounds[_foxWhines[whineIndex]], AudioData.SoundBus.Voice);
		animation = "Scared";
		UnflashFox(); // in case we are interrupting the animation in which the fox is modulated 10101010
		anim.Play(animation);
	}

	public void SetAnim(string animation)
	{

		int whineIndex = new Random().Next(0, _foxWhines.Count);
		// if (animation == "Baby")
		// {
		// 	return;
		// }
		// if (anim.IsPlaying() && animation == "Hurt")
		// {
		// 	return;
		// }
		if (anim.IsPlaying())
		{
			return;
		}
		string nextAnim = "Idle";
		if (awake)
		{
			if (Health<50)
			{
				NoInterruptVoiceSound(_foxSounds[_foxWhines[whineIndex]]);
				// GD.Print("TEST SICK ANIM");
				// AudioHandler.PlaySound(_soundPlayer, _foxSounds[_foxWhines[whineIndex]], AudioData.SoundBus.Voice);
				nextAnim = "Sick";
			}
			else if (Satiety<50)
			{
				int hungryIndex = new Random().Next(0, _foxHungry.Count);
				NoInterruptVoiceSound(_foxSounds[_foxHungry[hungryIndex]]);
				nextAnim = "Hungry";
			}
			else if (Happiness<30)
			{
				NoInterruptVoiceSound(_foxSounds[_foxWhines[whineIndex]]);
				nextAnim = "Sad";
			}
			// else if (Happiness>50)
			// {
			// 	animation = "Idle";
			// }
		}
		if (!awake)// && !_sleepingAnims.ToList().Contains(animation))
		{
			int index = new Random().Next(0, _sleepingAnims.Length);
			nextAnim = _sleepingAnims[index];
			// animation = "Sleeping";
		}
		// if (currentAnim == animation)
		// {
		// 	return;
		// }
		// Modulate = new Color(1,1,1,1);
		// currentAnim = animation;	
		anim.Play(nextAnim);
		// GD.Print(animation);
	}

	private void NoInterruptVoiceSound(AudioStream stream)
	{
		if (_soundPlayer.Playing)
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, stream, AudioData.SoundBus.Voice);
	}

	private void OnAnimationFinished(String animation)
	{
		// if (animation == "Baby")
		// {
		// 	currentAnim = "Idle";
		// 	anim.Play(currentAnim);
		// 	return;
		// }
		GD.Print(foxID, " ", animation);
		UnflashFox(); // this may not be needed now that we dont try to change animation every frame...
		SetAnim(animation);
		// if (animation == "Sleeping")
		// {
		// 	// GD.Print("anim finished");
		// 	return;	
		// }
		// currentAnim = "Idle";
		// anim.Play(currentAnim);
	}

	public void OnFoxDidPoo()
	{
	}

	private void OnMouseEntered()
	{
		mouseEntered = true;
	}

	private void OnMouseExited()
	{
		mouseEntered = false;
	}
	
	private List<AudioData.FoxSounds> _foxLaughs = new List<AudioData.FoxSounds>{ AudioData.FoxSounds.Laugh1, AudioData.FoxSounds.Laugh2, 
		AudioData.FoxSounds.Laugh3, AudioData.FoxSounds.Laugh4};
	private List<AudioData.FoxSounds> _foxWhines = new List<AudioData.FoxSounds>{ AudioData.FoxSounds.Whine1, AudioData.FoxSounds.Whine2, 
		AudioData.FoxSounds.Whine3};
	private List<AudioData.FoxSounds> _foxHungry = new List<AudioData.FoxSounds>{ AudioData.FoxSounds.Hungry1, AudioData.FoxSounds.Hungry2};

	public void OnPetFox()
	{
		if (_currentSpeed == 0 || anim.CurrentAnimation == "Baby")
		{
			return;
		}
		PetFox?.Invoke();
		timer.Start();
		animation = "Happy";
		anim.Play(animation);
		MakeFoxHappy(5,10);
		int index = new Random().Next(0, _foxLaughs.Count);
		// MakeVoiceSound(_foxSounds[_foxWhines[whineIndex]]);
		AudioHandler.PlaySound(_soundPlayer, _foxSounds[_foxLaughs[index]], AudioData.SoundBus.Voice);
		// PetFox?.Invoke();

	}

	private void OnFoxGUIInput(InputEvent ev)
	{
		if (ev.IsPressed())
		{
			if (ev is InputEventMouseButton btn)
			{
				if (btn.ButtonIndex == (int)ButtonList.Left && timer.IsStopped())
				{
					OnPetFox();
					// GD.Print("Pet fox");
				}
			}
		}
	}
}
