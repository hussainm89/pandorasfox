using Godot;
using System;

public class Food : Control, IInventoriable
{

	private InventoryItem _inventoryItem;
	public InventoryItem InventoryItem
	{
		get
		{
			return _inventoryItem;
		}
		set
		{
			_inventoryItem = value;
		}
	}
	public void Init(InventoryItem inventoryItem)
	{
		this.InventoryItem = inventoryItem;
		this.InventoryItem.Cost = 100;
	}

	public float Health {get; set;} = 100;

	public event InventoryItem.ItemUsedDelegate ItemUsed;

	public delegate void FoodUsedDelegate(int itemID);
	public event FoodUsedDelegate FoodUsed;

	private string _itemName = "Food";
	public string ItemName
	{
		get
		{
			return _itemName;
		}
		set
		{
			_itemName = value;
		}
	}

	public void Use()
	{
		ItemUsed?.Invoke(InventoryItem.ItemID);
		FoodUsed?.Invoke(InventoryItem.ItemID);
	}

}
